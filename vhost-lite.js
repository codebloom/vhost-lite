var http = require('http');
var httpProxy = require('http-proxy');
var fs = require('fs');
var vhttps = require('vhttps');
require('dotenv').config();

var config = JSON.parse(fs.readFileSync('config.json', 'utf8'));

var hosts = config.hosts.map((config_host) => {
	var crtProxy = new httpProxy.createProxyServer({
		target: {
			host: 'localhost',
			port: config_host.port
		}
	});

	return {
		proxy: crtProxy,
		matchRegex : new RegExp(config_host.matchRegex, 'i'),
		name: config_host.name,
		port: config_host.port,
		certPaths: config_host.certPaths
	}
});



/**
 * Handle simple http
 */
http.createServer(function(req, res) {

	var matchedHost = hosts.reduce((acc, crtHost) => {
		return req.headers.host.match( crtHost.matchRegex ) ? crtHost : acc;
	}, false);

	// this host has https configured
	if (matchedHost.certPaths.cert && matchedHost.certPaths.key) {


		res.writeHead(302, {'Location': 'https://' + req.headers.host.replace(/^www\./, '') + req.url});
		res.end();
		return;
	}


	// this host does not have https configured, proceed with http
	if (matchedHost) {

		matchedHost.proxy.proxyRequest(req, res);



		matchedHost.proxy.on('error', (err) => {

                      console.log(req, res);
			if (err) console.log(err);
			res.writeHead(500);
			res.end(`Something Went really wrong trying to proxy ${matchedHost.name} on port ${matchedHost.port}. \n\n ${err.message}`);
		});
	} else {

		res.writeHead(500);
		res.end(`We weren't able to find a find any proxy that matches the host ${req.headers.host}`);
	}
}).listen(process.env.PORT);



/**
 * Handle https
 */
const defaultCredential = {
	cert: "",
	key: ""
};

const credentialArray = config.hosts.map(function(config_host){
	var cert = config_host.certPaths.cert ? fs.readFileSync(config_host.certPaths.cert) : '';
	var key = config_host.certPaths.key ? fs.readFileSync(config_host.certPaths.key) : '';
	var ca = config_host.certPaths.ca ? fs.readFileSync(config_host.certPaths.ca) : '';
    return {
		hostname: config_host.domain,
		cert: cert + ca,
		key: key,
		requestCert: true,
		rejectUnauthorized: false
    }
});



const httpsServer = vhttps.createServer(defaultCredential, credentialArray, function(req, res) {


        var matchedHost = hosts.reduce((acc, crtHost) => {
                return req.headers.host.match( crtHost.matchRegex ) ? crtHost : acc;
        }, hosts[0]);


        if (matchedHost) {
                matchedHost.proxy.proxyRequest(req, res);
                matchedHost.proxy.on('error', (err) => {
                        if (err) console.log(err);
                        res.writeHead(500);
                        res.end(`Something Went really wrong trying to proxy ${matchedHost.name} on port ${matchedHost.port}. \n\n ${err.message}`);
                });
        } else {

                res.writeHead(500);
                res.end(`We weren't able to find a find any proxy that matches the host ${req.headers.host}`);
        }


});

httpsServer.listen(process.env.PORTHTTPS);

console.log('===== server started on port: ' + process.env.PORT + ' and port: ' + process.env.PORTHTTPS + ' ===========');

